﻿using System.ComponentModel.DataAnnotations;

namespace Smart_Garage.Models
{
    public class Service
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Labour { get; set; } // TODO: change "Labour" to "Name"

        [Required]
        [Range(0, int.MaxValue)]
        public int Price { get; set; }
        public int VehicleId { get; set; }
        public Vehicle Vehicle { get; set; }
    }
}
