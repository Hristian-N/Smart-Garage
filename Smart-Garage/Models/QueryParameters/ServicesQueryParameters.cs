﻿namespace Smart_Garage.Models.QueryParameters
{
    public class ServicesQueryParameters
    {
        public string? Labour { get; set; }
        public int? Price { get; set; }
    }
}
