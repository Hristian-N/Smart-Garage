﻿namespace Smart_Garage.Models.QueryParameters
{
    public class UserQueryParameters
    {
        public string? Username { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }
    }
}
